package com.example.timelog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by revant on 21/3/17.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent){
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            // Start your intent service
            Intent i = new Intent(context, ToDoSyncService.class);
            context.startService(i);
        }
    }
}

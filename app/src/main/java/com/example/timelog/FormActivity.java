package com.example.timelog;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FormActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback {
    AccountManager am;
    Account[] accounts;
    Account account;
    String accountname;
    String authToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        am = AccountManager.get(this);
        String accountname = getIntent().getStringExtra("ACCOUNT_NAME");
        accounts = am.getAccountsByType("io.frappe.frappeauthenticator");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(FormActivity.this,
                    new String[]{Manifest.permission.GET_ACCOUNTS},
                    PackageManager.PERMISSION_GRANTED);
            return;
        }

        for (final Account a : accounts) {
            if (a.name.equals(accountname)) {
                account = a;
                am.getAuthToken(a, "Full access", null, true, new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        try {
                            Bundle bundle = future.getResult();
                            for (String key : bundle.keySet()) {
                                Object value = bundle.get(key);
                            }
                            authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                            try {
                                final JSONObject bearerToken = new JSONObject(authToken);
                                final String frappeServerURL = am.getUserData(a, "frappeServer");
                                getEmployeeFromUser(frappeServerURL, a.name, bearerToken.getString("access_token"));
                                getTasksforUser(frappeServerURL, "Task", a.name, bearerToken.getString("access_token"));
                                getActivityType(frappeServerURL, "Activity Type", bearerToken.getString("access_token"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            Log.d("error", e.getMessage());
                        }
                    }
                }, null);
            }
        }

        Button cancelButton = (Button) findViewById(R.id.bCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button startStopButton = (Button) findViewById(R.id.bStart);
        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner sTimesheet = (Spinner) findViewById(R.id.spinTimesheet);
                Spinner sTask = (Spinner) findViewById(R.id.spinTask);
                Spinner sActivity = (Spinner) findViewById(R.id.spinActType);

                if (sTask.getSelectedItem() != null && sTask.getSelectedItem().toString().isEmpty()) {
                    new AlertDialog.Builder(FormActivity.this)
                            .setTitle("Select Task")
                            .setMessage("Please select Task")
                            .show();
                } else if (sActivity.getSelectedItem() != null && sActivity.getSelectedItem().toString().isEmpty()) {
                    new AlertDialog.Builder(FormActivity.this)
                            .setTitle("Select Activity Type")
                            .setMessage("Please select Activity Type")
                            .show();
                }
                try {
                    addTimelog(sTimesheet.getSelectedItem().toString(),
                            sTask.getSelectedItem().toString(),
                            sActivity.getSelectedItem().toString());
                } catch (NullPointerException e) {
                    new AlertDialog.Builder(FormActivity.this)
                            .setTitle("Select All Fields")
                            .setMessage("Please select all fields")
                            .show();
                }
            }
        });
    }

    private void addTimelog(final String timesheet, final String task, final String actType) {
        final ERPNextDataProvider server = new ERPNextDataProvider();
        if (account!=null){
            am.getAuthToken(account, "Full access", null, true, new AccountManagerCallback<Bundle>() {
                @Override
                public void run(AccountManagerFuture<Bundle> future) {
                    try {
                        Bundle bundle = future.getResult();
                        for (String key : bundle.keySet()) {
                            Object value = bundle.get(key);
                            Log.d("callback", String.format("%s %s", key, value.toString()));
                        }
                        authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                        try {
                            final JSONObject bearerToken = new JSONObject(authToken);
                            final String frappeServerURL = am.getUserData(account, "frappeServer");

                            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date today = Calendar.getInstance().getTime();
                            String nowTime = dt.format(today);

                            JSONObject data = new JSONObject();
                            data.put("parent", timesheet);
                            data.put("parenttype", "Timesheet");
                            data.put("parentfield", "time_logs");
                            data.put("activity_type", actType);
                            data.put("task", task);
                            data.put("from_time", nowTime);
                            data.put("to_time", nowTime);

                            server.newDoc(frappeServerURL,bearerToken.getString("access_token"),"Timesheet Detail", data,
                                    new FrappeServerPOSTCallback() {
                                        @Override
                                        public void onSuccessString(String response) {
                                            finish();
                                        }
                                    });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        Log.d("error", e.getMessage());
                    }
                }
            }, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PackageManager.PERMISSION_GRANTED: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    for (Account a : accounts) {
                        if (a.toString().equalsIgnoreCase(accountname)) {

                        }
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getDraftTimeSheetForEmployee(final String frappeServerURL, String employee, final String access_token) {
        try {
            employee = URLEncoder.encode(employee, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ERPNextDataProvider server = new ERPNextDataProvider();
        server.getAll(frappeServerURL, access_token, "Timesheet",
                "{\"docstatus\":0,\"employee\":\"" + employee + "\"}", "[\"*\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        JSONArray timesheets = new JSONArray();
                        try {
                            timesheets = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Add latest draft timesheet to spinner
                        Spinner spinner = (Spinner) findViewById(R.id.spinTimesheet);
                        ArrayAdapter<String> adapter;
                        List<String> list;
                        list = new ArrayList<String>();

                        JSONObject object = null;
                        try {
                            object = timesheets.getJSONObject(0);
                            list.add(object.getString("name"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, list);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        spinner.setSelection(0);

                    }
                });
    }

    private void getTasksforUser(final String frappeServerURL, String task, String user, final String access_token) {
        try {
            task = URLEncoder.encode(task, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("[[\"status\",\"not%20in\",[\"Closed\",\"Cancelled\"]],[\"_assign\",\"like\",\"%"
                + user + "%\"]]");
        ERPNextDataProvider server = new ERPNextDataProvider();
        server.getAll(frappeServerURL, access_token, "Task",
                "[[\"status\",\"not%20in\",[\"Closed\",\"Cancelled\"]],[\"_assign\",\"like\",\"%25"
                        + user + "%25\"]]", "[\"*\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        JSONArray tasks = new JSONArray();
                        try {
                            tasks = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Add tasks to spinner
                        Spinner spinner = (Spinner) findViewById(R.id.spinTask);
                        ArrayAdapter<String> adapter;
                        List<String> list;
                        list = new ArrayList<String>();
                        list.add("");

                        for (int i = 0; i < tasks.length(); i++) {
                            JSONObject object = null;
                            try {
                                object = tasks.getJSONObject(i);
                                list.add(object.getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, list);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        spinner.setSelection(0);
                    }
                });
    }

    private void getActivityType(final String frappeServerURL, String docType, final String access_token) {
        try {
            docType = URLEncoder.encode(docType, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ERPNextDataProvider server = new ERPNextDataProvider();
        server.getAll(frappeServerURL, access_token, docType,
                null, "[\"*\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        JSONArray tasks = new JSONArray();
                        try {
                            tasks = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Add timesheets to spinner
                        Spinner spinner = (Spinner) findViewById(R.id.spinActType);
                        ArrayAdapter<String> adapter;
                        List<String> list;
                        list = new ArrayList<String>();
                        list.add("");

                        for (int i = 0; i < tasks.length(); i++) {
                            JSONObject object = null;
                            try {
                                object = tasks.getJSONObject(i);
                                list.add(object.getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, list);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        spinner.setSelection(0);
                    }
                });
    }

    public void getEmployeeFromUser(final String frappeServerURL, String user, final String access_token) {
        try {
            user = URLEncoder.encode(user, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ERPNextDataProvider server = new ERPNextDataProvider();
        server.getAll(frappeServerURL, access_token, "Employee",
                "{\"user_id\":\"" + user + "\"}", "[\"name\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        JSONArray employee = new JSONArray();
                        try {
                            employee = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < employee.length(); i++) {
                            JSONObject object = null;
                            try {
                                object = employee.getJSONObject(i);
                                getDraftTimeSheetForEmployee(frappeServerURL, object.getString("name"), access_token);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }
}

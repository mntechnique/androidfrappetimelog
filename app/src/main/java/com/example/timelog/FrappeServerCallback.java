package com.example.timelog;

import org.json.JSONObject;

/**
 * Created by revant on 21/3/17.
 */

public interface FrappeServerCallback{
    public void onSuccessJSONObject(JSONObject result);
}

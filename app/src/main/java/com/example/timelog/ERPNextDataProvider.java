package com.example.timelog;

/**
 * Created by revant on 16/3/17.
 */
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by revant on 26/2/17.
 */

public class ERPNextDataProvider {
    JSONArray out;
    public JSONArray getAll(String frappeServerURL, final String access_token, String docType,
                            String filters, String fields, final FrappeServerCallback callback) {

        // Post params to be sent to the server
        // HashMap<String, String> params = new HashMap<String, String>();
        // params.put("limit_page_length","None");
        String endpoint = frappeServerURL + "/api/resource/" + docType + "?limit_page_length=None&";
        if (fields == null){
            endpoint += "filters="+filters;
        } else if (filters == null){
            endpoint += "fields="+fields;
        } else if (filters != null && fields != null){
            endpoint += "fields="+ fields + "&filters=" + filters;
        }
        final String ep = endpoint;
        JsonObjectRequest req = new JsonObjectRequest(endpoint, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccessJSONObject(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                //..add other headers
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }
        };

        // add the request object to the queue to be executed
        ApplicationController.getInstance().addToRequestQueue(req);
        return out;
    }

    public void newDoc(String frappeServerURL, final String access_token, String docType,
                       final JSONObject data, final FrappeServerPOSTCallback callback) {
        String endpoint = null;
        try {
            endpoint = frappeServerURL + "/api/resource/" + URLEncoder.encode(docType, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringRequest req = new StringRequest(Request.Method.POST, endpoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccessString(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.DEBUG = true;
                String errorMessage = null;
                try {
                    errorMessage = new String (error.networkResponse.data,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                System.out.println(errorMessage);
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                //..add other headers
                //params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                //..add other headers
                //params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("data", data.toString());
                System.out.println(params);
                return params;
            }
        };

        // add the request object to the queue to be executed
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public void updateDoc(String frappeServerURL, final String access_token, String docType, String docName,
                          final JSONObject data, final FrappeServerPOSTCallback callback) {
        String endpoint = null;
        try {
            endpoint = frappeServerURL + "/api/resource/" +
                    URLEncoder.encode(docType, "UTF-8").replace("+", "%20") + "/" +
                    URLEncoder.encode(docName, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringRequest req = new StringRequest(Request.Method.PUT, endpoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccessString(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.DEBUG = true;
                String errorMessage = null;
                try {
                    errorMessage = new String (error.networkResponse.data,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                System.out.println(errorMessage);
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                //..add other headers
                //params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                //..add other headers
                //params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("data", data.toString());
                System.out.println(params);
                return params;
            }
        };

        // add the request object to the queue to be executed
        ApplicationController.getInstance().addToRequestQueue(req);
    }

}
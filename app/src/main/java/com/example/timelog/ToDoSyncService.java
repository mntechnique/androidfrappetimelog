package com.example.timelog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by revant on 21/3/17.
 */

public class ToDoSyncService extends Service {
    String authToken;
    // default interval for syncing data
    public static final long DEFAULT_SYNC_INTERVAL = 30 * 1000;

    // task to be run here
    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            showNotificationforAllAccounts();
            // Repeat this runnable code block again every ... min
            new IncomingHandler().postDelayed(runnableService, DEFAULT_SYNC_INTERVAL);
        }
    };

    private void showNotificationforAllAccounts(){
        final AccountManager mAccountManager = AccountManager.get(this);
        Account[] accounts = mAccountManager.getAccountsByType("io.frappe.frappeauthenticator");
        final ERPNextDataProvider server = new ERPNextDataProvider();
        // adding a section and items into it
        for (final Account account : accounts) {
            String frappeServerURL = mAccountManager.getUserData(account,"frappeServer");
            mAccountManager.getAuthToken(account, "Full access", null, true, new AccountManagerCallback<Bundle>() {
                @Override
                public void run(AccountManagerFuture<Bundle> future) {
                    try {
                        Bundle bundle = future.getResult();
                        for (String key : bundle.keySet()) {
                            Object value = bundle.get(key);
                            Log.d("callback", String.format("%s %s", key, value.toString()));
                        }
                        authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                        try {
                            final JSONObject bearerToken = new JSONObject(authToken);
                            final String frappeServerURL = mAccountManager.getUserData(account, "frappeServer");
                            server.getAll(frappeServerURL, bearerToken.getString("access_token"), "ToDo",
                                    "{\"owner\":\""+ account.name+"\",\"status\":\"Open\"}", "[\"name\"]", new FrappeServerCallback() {
                                        @Override
                                        public void onSuccessJSONObject(JSONObject response) {
                                            JSONArray todos = new JSONArray();
                                            try {
                                                todos = response.getJSONArray("data");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            String todoLen = Integer.toString(todos.length());
                                            showNotification(account,todoLen);
                                        }
                                    });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mAccountManager.invalidateAuthToken(account.type, authToken);
                    } catch (Exception e) {
                        Log.d("error", e.getMessage());
                    }
                }
            },null);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create the Handler object
        IncomingHandler mHandler = new IncomingHandler();
        // Execute a runnable task as soon as possible
        mHandler.post(runnableService);

        return START_STICKY;
    }

    /** For showing and hiding our notification. */
    NotificationManager mNM;
    /** Keeps track of all current registered clients. */
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    /** Holds last value set by a client. */
    int mValue = 0;

    /**
     * Command to the service to register a client, receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client where callbacks should be sent.
     */
    static final int MSG_REGISTER_CLIENT = 1;

    /**
     * Command to the service to unregister a client, ot stop receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client as previously given with MSG_REGISTER_CLIENT.
     */
    static final int MSG_UNREGISTER_CLIENT = 2;

    /**
     * Command to service to set a new value.  This can be sent to the
     * service to supply a new value, and will be sent by the service to
     * any registered clients with the new value.
     */
    static final int MSG_SET_VALUE = 3;

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    mValue = msg.arg1;
                    for (int i=mClients.size()-1; i>=0; i--) {
                        try {
                            mClients.get(i).send(Message.obtain(null,
                                    MSG_SET_VALUE, mValue, 0));
                        } catch (RemoteException e) {
                            // The client is dead.  Remove it from the list;
                            // we are going through the list from back to front
                            // so this is safe to do inside the loop.
                            mClients.remove(i);
                        }
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        // Display a notification about us starting.
        //showNotification();
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        mNM.cancel(R.string.remote_service_started);

        // Tell the user we stopped.
        Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();
    }

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification(Account account, String toDoLen) {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = account.name;

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)  // the status icon
                .setTicker(text)  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(toDoLen + " Open ToDo(s)")  // the label of the entry
                .setContentText(text)  // the contents of the entry
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                .build();

        // Send the notification.
        // We use a string id because it is a unique number.  We use it later to cancel.
        mNM.notify(R.string.remote_service_started, notification);
    }
}

package com.example.timelog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends Activity
        implements NavigationView.OnNavigationItemSelectedListener {
    AccountManager mAccountManager;
    Account account;
    String authToken;
    ListView lv = null;
    String timeSheet;
    String accountname;
    Intent myIntent;
    ERPNextDataProvider server = new ERPNextDataProvider();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mAccountManager = AccountManager.get(this);
        accountname = getIntent().getStringExtra("ACCOUNT_NAME");
        Account[] accounts = mAccountManager.getAccountsByType("io.frappe.frappeauthenticator");
        for(final Account a : accounts) {
            if(a.name.equals(accountname)) {
                account = a;
                mAccountManager.getAuthToken(a, "Full access", null, true, new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        try {
                            Bundle bundle = future.getResult();
                            for (String key : bundle.keySet()) {
                                Object value = bundle.get(key);
                                Log.d("callback", String.format("%s %s", key, value.toString()));
                            }
                            authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                            try {
                                final JSONObject bearerToken = new JSONObject(authToken);
                                final String frappeServerURL = mAccountManager.getUserData(a, "frappeServer");
                                getEmployeeFromUser(frappeServerURL,a.name,bearerToken.getString("access_token"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("access_token", authToken);
                            mAccountManager.invalidateAuthToken(a.type, authToken);
                        } catch (Exception e) {
                            Log.d("error", e.getMessage());
                        }
                    }
                },null);
            }
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        if(accountname!=null) fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (account != null) {
                    myIntent = new Intent(MainActivity.this,
                            FormActivity.class);
                    myIntent.putExtra("ACCOUNT_NAME", account.name);
                    startActivity(myIntent);
                }
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        addItemsRunTime(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        // int id = item.getItemId();

        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra("ACCOUNT_NAME", item.toString());
        startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void addItemsRunTime(NavigationView navigationView) {

        //adding items run time
        final Menu menu = navigationView.getMenu();

        Account[] accounts = mAccountManager.getAccountsByType("io.frappe.frappeauthenticator");
        final SubMenu subMenu = menu.addSubMenu("Accounts");
        // adding a section and items into it
        for (Account account : accounts) {
            subMenu.add(account.name);
        }
        // refreshing navigation drawer adapter
        for (int i = 0, count = navigationView.getChildCount(); i < count; i++) {
            final View child = navigationView.getChildAt(i);
            if (child != null && child instanceof ListView) {
                final ListView menuView = (ListView) child;
                final HeaderViewListAdapter adapter = (HeaderViewListAdapter) menuView.getAdapter();
                final BaseAdapter wrapped = (BaseAdapter) adapter.getWrappedAdapter();
                wrapped.notifyDataSetChanged();
            }
        }
    }
    private void getDraftTimeSheetForEmployee(final String frappeServerURL, String employee, final String access_token){
        try {
            employee = URLEncoder.encode(employee, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        server.getAll(frappeServerURL, access_token, "Timesheet",
                "{\"docstatus\":0,\"employee\":\""+employee+"\"}", "[\"*\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        lv = (ListView) findViewById(R.id.listMain);

                        List<String> timesheetLV = new ArrayList<String>();
                        JSONArray timesheets = new JSONArray();
                        try {
                            timesheets = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JSONObject object = null;
                        try {
                            object = timesheets.getJSONObject(0);
                            timesheetLV.add(object.getString("name"));
                            getTimeSheetDetailForTimesheet(frappeServerURL,object.getString("name"),access_token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
    private void getTimeSheetDetailForTimesheet(final String frappeServerURL, String timesheet, final String access_token){
        try {
            timesheet = URLEncoder.encode(timesheet, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        server.getAll(frappeServerURL, access_token, "Timesheet%20Detail",
                "{\"parent\":\""+timesheet+"\",\"hours\":0.0}", "[\"*\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        lv = (ListView) findViewById(R.id.listMain);

                        List<String> timesheetLV = new ArrayList<String>();
                        JSONArray timesheets = new JSONArray();
                        try {
                            timesheets = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for(int i=0;i<timesheets.length();i++){
                            JSONObject object = null;
                            try {
                                object = timesheets.getJSONObject(i);
                                timesheetLV.add(object.getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                                MainActivity.this,
                                android.R.layout.simple_expandable_list_item_1,
                                timesheetLV);

                        lv.setAdapter(arrayAdapter);
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                    long id) {
                                final String timeLog = ((TextView)view).getText().toString();
                                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date today = Calendar.getInstance().getTime();
                                String nowTime = dt.format(today);
                                JSONObject data = new JSONObject();
                                try {
                                    data.put("to_time", nowTime);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                server.updateDoc(frappeServerURL,access_token,"Timesheet Detail", timeLog, data,
                                        new FrappeServerPOSTCallback() {
                                            @Override
                                            public void onSuccessString(String response) {
                                                JSONObject data = new JSONObject();
                                                try {
                                                    data = new JSONObject(response);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                JSONObject updatedTL = new JSONObject();
                                                try {
                                                    updatedTL = data.getJSONObject("data");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                JSONObject hours = new JSONObject();
                                                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                Date fromTime = new Date();
                                                Date toTime = new Date();
                                                try {
                                                    fromTime = dt.parse(updatedTL.getString("from_time"));
                                                    toTime = dt.parse(updatedTL.getString("to_time"));
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                float hourDiff = ((float)(toTime.getTime()-fromTime.getTime())/1000/3600);
                                                System.out.println(toTime);
                                                System.out.println(fromTime);
                                                System.out.println(hourDiff);
                                                try {
                                                    hours.put("hours", hourDiff);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                server.updateDoc(frappeServerURL,access_token,"Timesheet Detail", timeLog, hours,
                                                        new FrappeServerPOSTCallback() {
                                                            @Override
                                                            public void onSuccessString(String response) {

                                                            }
                                                        });
                                                finish();
                                            }
                                        });
                            }
                        });
                    }
                });
    }

    public void getEmployeeFromUser(final String frappeServerURL, String user, final String access_token){
        try {
            user = URLEncoder.encode(user, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        server.getAll(frappeServerURL, access_token, "Employee",
                "{\"user_id\":\""+ user +"\"}", "[\"name\"]", new FrappeServerCallback() {
                    @Override
                    public void onSuccessJSONObject(JSONObject response) {
                        JSONArray employee = new JSONArray();
                        try {
                            employee = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for(int i=0;i<employee.length();i++){
                            JSONObject object = null;
                            try {
                                object = employee.getJSONObject(i);
                                getDraftTimeSheetForEmployee(frappeServerURL, object.getString("name"), access_token);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }
}
